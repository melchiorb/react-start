import { combineReducers } from 'redux'
import appState from './app'

const rootReducer = combineReducers({
  appState
})

export default rootReducer
