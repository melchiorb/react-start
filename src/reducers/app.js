import { MAIN } from '../constants/actionTypes'
import objectAssign from 'object-assign'

const initialState = {
	foo: 0
}

export default function appState(state = initialState, action) {
	const apply = (values) => objectAssign({}, state, values)
	
	switch (action.type) {
		case MAIN:
			return apply({foo: (state.foo + 1)})
		default:
			return state
	}
}
