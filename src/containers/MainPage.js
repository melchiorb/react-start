import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions/mainActions'

export class MainPage extends Component {
  render() {
    return (
      <div>
        <div>{this.props.state.foo}</div>
        <button onClick={this.props.actions.mainAction}>Test</button>
      </div>
    )
  }
}

MainPage.propTypes = {
  actions: PropTypes.object.isRequired,
  state: PropTypes.object.isRequired
}

function mapStateToProps(state) {
  return {
    state: {
      foo: state.appState.foo
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPage)
