import * as types from '../constants/actionTypes'

export function mainAction(options) {
	return { type: types.MAIN, options }
}
